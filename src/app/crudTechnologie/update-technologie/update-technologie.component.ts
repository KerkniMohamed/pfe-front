import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import {Apollo} from 'apollo-angular';
import {ToastrService} from 'ngx-toastr';
import {Technologie} from '../../technologie.model';
import {consoleTestResultHandler} from 'tslint/lib/test';
import {updateTechnologie} from '../../shared/technologie/mutation';

@Component({
  selector: 'app-update-technologie',
  templateUrl: './update-technologie.component.html',
  styleUrls: ['./update-technologie.component.css']
})
export class UpdateTechnologieComponent implements OnInit {
  id: number ;
  libele: string ;
  @Input() technologie: Technologie ;
  @Output() updateTechnologie: EventEmitter<any>;
  constructor(private apollo: Apollo, private toastr: ToastrService) {
    this.updateTechnologie = new EventEmitter();
  }
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('userForm') form: any;
  ngOnInit() {
    this.libele = this.technologie.libele;
  }

  private closeModal(): void {
    this.closeAddExpenseModal.nativeElement.click();

  }
  onSubmit() {

    this.apollo.mutate({
      mutation: updateTechnologie,
      variables:  {id: parseInt(this.technologie.id.toString(), 10),
      libele: this.libele
      }


    })
        .subscribe(result => {
            console.log(result);
          this.toastr.success('Technologie modifié avec succées', 'Succées');
          this.technologie.libele = this.libele;
          this.updateTechnologie.emit(this.technologie);
        });
  }

}
