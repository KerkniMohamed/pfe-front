import {Component, EventEmitter, Input, OnInit, Output, ViewChild , ElementRef} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {addTechnologie} from '../../shared/technologie/mutation';
import {Toast, ToastrService} from 'ngx-toastr';
import {Technologie} from '../../technologie.model';

@Component({
  selector: 'app-add-technologie',
  templateUrl: './add-technologie.component.html',
  styleUrls: ['./add-technologie.component.css']
})
export class AddTechnologieComponent implements OnInit {
  libele: string ;
  @ViewChild('userForm') form: any;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  @Output() ajouterTechnologie: EventEmitter<any>;

  constructor(private apollo: Apollo , private toastr: ToastrService) {
    this.ajouterTechnologie = new EventEmitter();

  }
l
  ngOnInit() {

  }
private closeModal(): void {
  this.closeAddExpenseModal.nativeElement.click();

}
  onSubmit() {
        this.apollo.mutate({
          mutation: addTechnologie ,
            variables: {libele: this.libele } })
           .subscribe(
          result => {
              console.log(result.data.saveTechnologie);
            const techno = new Technologie();
            techno.id = result.data.saveTechnologie.id ;
            techno.libele = result.data.saveTechnologie.libele ;
            this.toastr.success('Technologie ajouté avec succées', 'Succées');
            this.ajouterTechnologie.emit(techno);
          }
      );
    }
}
