import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Technologie} from '../../technologie.model';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {log} from 'util';
import {User} from '../../user.model';
import {ToastrService} from 'ngx-toastr';
import {deleteTechnologie} from '../../shared/technologie/mutation';

@Component({
    selector: 'app-delete-technologie',
    templateUrl: './delete-technologie.component.html',
    styleUrls: ['./delete-technologie.component.css']
})
export class DeleteTechnologieComponent implements OnInit {
    @Input() technologie: Technologie;
    @Output() deleteTechnologie: EventEmitter<any>;
    constructor(private apollo: Apollo, private toastr: ToastrService) {
        this.deleteTechnologie = new EventEmitter();
    }

    ngOnInit() {
    }

    ondelete() {
        this.apollo.mutate({
            mutation: deleteTechnologie,
            variables:
                {id: parseInt(this.technologie.id.toString(), 10)}
        })
            .subscribe(result => {
                console.log(this.technologie);
                this.toastr.success('Technologie supprimé avec Succès', 'Succès');
                this.deleteTechnologie.emit(this.technologie);
            });
    }
}
