import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTechnologieComponent } from './delete-technologie.component';

describe('DeleteTechnologieComponent', () => {
  let component: DeleteTechnologieComponent;
  let fixture: ComponentFixture<DeleteTechnologieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTechnologieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTechnologieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
