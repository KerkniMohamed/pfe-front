import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import gql from 'graphql-tag';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import {Apollo} from 'apollo-angular';
import {User} from '../user.model';
import {Technologie} from '../technologie.model';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {HttpClientModule} from '@angular/common/http';
import {HttpLink} from 'apollo-angular-link-http';
import {Subject} from 'rxjs';
import { DeleteTechnologieComponent } from '../crudTechnologie/delete-technologie/delete-technologie.component' ;
import {DataTableDirective, DataTablesModule} from 'angular-datatables';
import {listeTechnologie} from '../shared/technologie/query';


@Component({
  selector: 'app-technologie',
  templateUrl: './technologie.component.html',
  styleUrls: ['./technologie.component.css']
})
export class TechnologieComponent implements AfterViewInit, OnDestroy, OnInit {

  id: number;
  libele: string;
  Technologies: any [] = [];
  public dtOptions: any = {};
  public dtTrigger: Subject<any> = new Subject();

  constructor(private apollo: Apollo) {
  }
  @ViewChild(DataTableDirective)  dtElement: DataTableDirective;

  ngOnInit(): void {
    this.dtOptions = {
      pageLength: 5,
      lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, 'All']],
      language: {
        lengthMenu: '_MENU_ Enregistrements par page',
        zeroRecords: 'Aucun enregistrements correspondants trouvés',
        info: 'Afficher page PAGE à _PAGES_',
        infoEmpty: 'Aucun enregistrement disponible',
        infoFiltered: '(filtré à partir du MAX enregistrements)',
        search: '',
        searchPlaceholder: 'recherche',
        paginate: {
          previous: '<',
          next: '>'
        }
      }
    };
    this.apollo.watchQuery<any>({
      query: listeTechnologie
    })
        .valueChanges
        .subscribe(result => {
          console.log(result.data.getAllTechnologies);
          this.Technologies = result.data.getAllTechnologies;
          this.dtTrigger.next();
        });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  Delete(techn: Technologie): void {
    this.rerender(techn, true );
  }
  Add(techn: Technologie): void {
    this.rerender(techn , false );
  }
  Update(techn: Technologie): void {
    this.rerender(techn);
  }
  rerender(newData, deleteOper = false): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
// Destroy the table first
      dtInstance.destroy();

// Remove old data
      this.Technologies = this.Technologies.filter(val => val.id !== newData.id);
// Only if it isn't a delete operation
      if (!deleteOper) {
        this.Technologies.push(newData);
      }
// Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  ngAfterViewInit(): void {
  }

  }

