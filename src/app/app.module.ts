import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {GraphQLModule} from './graphql.module';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module' ;
import {RouterModule, Routes} from '@angular/router';
import {AuthService} from './service/auth.service';
import {NavBarComponent} from './interface/nav-bar/nav-bar.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {AddOffreComponent} from './crudOffre/add-offre/add-offre.component';
import { DeleteOffreComponent } from './crudOffre/delete-offre/delete-offre.component';

const appRoutes: Routes = [
  {
    path: 'interface',
    loadChildren: './interface/interface.module#InterfaceModule'
  },
  {path: 'login', component: LoginComponent},
  {path: 'addoffre', component: AddOffreComponent}

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
    BrowserModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-left',
      preventDuplicates: true,
    }),

  ],
  declarations: [
    AppComponent, LoginComponent, NavBarComponent, AddOffreComponent, DeleteOffreComponent
  ],

  providers: [AuthService],
  bootstrap: [AppComponent],
  exports: [RouterModule]

})
export class AppModule {
}
