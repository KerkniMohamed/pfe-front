import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterfaceComponent } from './interface.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {GraphQLModule} from '../graphql.module';
import {ApolloModule} from 'apollo-angular';
import {ListeoffreComponent} from '../listeoffre/listeoffre.component';
import {DataTablesModule} from 'angular-datatables';
import {TechnologieComponent} from '../technologie/technologie.component';
import {ModalModule} from 'ngx-bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import {ToastrModule} from 'ngx-toastr';
import { DeleteTechnologieComponent } from '../crudTechnologie/delete-technologie/delete-technologie.component' ;
import {AddTechnologieComponent} from '../crudTechnologie/add-technologie/add-technologie.component';
import {UpdateTechnologieComponent} from '../crudTechnologie/update-technologie/update-technologie.component';

const routes: Routes = [
  {path: 'offres' , component:  ListeoffreComponent},
  {path: 'technologie' , component: TechnologieComponent}
];

@NgModule({
  declarations: [ InterfaceComponent , ListeoffreComponent , TechnologieComponent ,
    DeleteTechnologieComponent , AddTechnologieComponent , UpdateTechnologieComponent ],
  imports: [
    CommonModule,
    RouterModule,
    DataTablesModule,
    FormsModule,
    GraphQLModule,
    ApolloModule,
    RouterModule.forChild(routes),
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),

  ],
  exports: [RouterModule]

})
export class InterfaceModule { }
