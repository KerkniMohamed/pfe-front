import { Component, OnInit } from '@angular/core';
import {Apollo} from 'apollo-angular';

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.css']
})
export class InterfaceComponent implements OnInit {

  constructor( private apollo: Apollo) {
  }

  ngOnInit() {
  }

}
