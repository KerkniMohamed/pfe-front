import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../service/auth.service';
import {DataTableDirective, DataTablesModule} from 'angular-datatables';
import {Apollo} from 'apollo-angular';
import {Subject} from 'rxjs';
import {getOffres} from '../shared/offre/query';
import {Offre} from '../offre.model';

@Component({
  selector: 'app-listeoffre',
  templateUrl: './listeoffre.component.html',
  styleUrls: ['./listeoffre.component.css']
})
export class ListeoffreComponent implements AfterViewInit, OnDestroy, OnInit {
  public OffreDetails: object = [];
  public dtOptions: any = {};
  public dtTrigger: Subject<any> = new Subject();

  Offres: any [] = [];
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  constructor(private authService: AuthService, private apollo: Apollo) {

  }


  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  ngOnInit(): void {
    // @ts-ignore
    this.dtOptions = {
      pageLength: 5,
      lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, 'All']],
      language: {
        lengthMenu: '_MENU_ Enregistrements par page',
        zeroRecords: 'Aucun enregistrements correspondants trouvés',
        info: 'Afficher page PAGE à _PAGES_',
        infoEmpty: 'Aucun enregistrement disponible',
        infoFiltered: '(filtré à partir du MAX enregistrements)',
        search: '',
        searchPlaceholder: 'recherche',
        paginate: {
          previous: '<',
          next: '>'
        }
      }
    };
    this.apollo.watchQuery<any>({
      query: getOffres
    })
      .valueChanges
      .subscribe(result => {
        console.log(result.data.getOffresRp);

        this.Offres = result.data.getOffresRp;

        this.dtTrigger.next();
      });

  }

  SearchOffre(etat: string) {
    const obj = this.Offres.filter(m => m.etat === etat);
    this.OffreDetails = obj;
    return this.OffreDetails;
  }

  ngAfterViewInit(): void {
  }

}

