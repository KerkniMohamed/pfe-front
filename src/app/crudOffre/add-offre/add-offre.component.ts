import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {User} from '../../user.model';
import {Apollo} from 'apollo-angular';
import {AddOffre} from '../../shared/offre/mutation';
import {Technologie} from '../../technologie.model';
import {listeTechnologie, login} from '../../shared/technologie/query';
import {Subject} from 'rxjs';
import {getOffres, rpFild} from '../../shared/offre/query';
import {Offre} from '../../offre.model';
import {ToastrService} from 'ngx-toastr';
import {Route, Router} from '@angular/router';

@Component({
  selector: 'app-add-offre',
  templateUrl: './add-offre.component.html',
  styleUrls: ['./add-offre.component.css']
})
export class AddOffreComponent implements OnInit {
  @Input() technologie: Technologie;


  Technologie: any [] = [];
  users: [] = [];
  libele: string;
  Offres: any [] = [];
  username: string;
  organisme: string;
  technologies: [Technologie];
  id: string ;
  user: [User];
  description: string ;
  diplome: string ;
  effectif: string ;
  etat: string ;
  exigence: string ;
  etude_besoin: string ;
  nb_annee_experience: string ;
  competance_demande: string ;
    @ViewChild('userForm') form: any;
  @Output() ajouterOffre: EventEmitter<any>;

  public dtTrigger: Subject<any> = new Subject();

  public auth: any;

  constructor(private apollo: Apollo, public authService: AuthService ,  private toastr: ToastrService, private route: Router) {
    this.auth = authService;
    this.ajouterOffre = new EventEmitter();

  }

  ngOnInit() {
    this.apollo.watchQuery<any>({
      query: listeTechnologie
    })
      .valueChanges
      .subscribe(result => {
        console.log(result.data.getAllTechnologies);
        this.Technologie = result.data.getAllTechnologies ;
      });
    this.apollo.watchQuery<any>({
      query: rpFild
    })
      .valueChanges
      .subscribe(result => {
        this.users = result.data.rpFild;
      });
  }

  onSubmit() {
    console.log(this.technologies);

    const  tab = this.technologies.map(function(e) {
     delete e.__typename;
      console.log(e);
      return e;
    });

/*
    const userTab = this.user.map(function (u) {

      delete u.__typename;

      console.log(u);
      return u;
    });
*/
    console.log(tab);
    console.log(this.user);
    this.apollo.mutate({
      mutation: AddOffre,
      variables: {
        organisme: this.organisme,
        description: this.description ,
        diplome: this.diplome ,
        effectif: this.effectif ,
        etat : this.etat ,
        exigence : this.exigence ,
        etude_besoin : this.etude_besoin ,
        nb_annee_experience: this.nb_annee_experience ,
        competance_demande: this.competance_demande ,
        technologies : tab,
        user: this.user ,
      }
    })
        .subscribe(result => {
    const  offre = new Offre() ;
   offre.organisme = result.data.saveOffreRp.organisme;
    offre.description = result.data.saveOffreRp.description;
    offre.diplome = result.data.saveOffreRp.diplome;
    offre.effectif = result.data.saveOffreRp.effectif;
    offre.etat = result.data.saveOffreRp.etat;
    offre.etude_besoin = result.data.saveOffreRp.etude_besoin;
    offre.exigence = result.data.saveOffreRp.effectif;
    offre.nb_annee_experience = result.data.saveOffreRp.nb_annee_experience;
    offre.competance_demande = result.data.saveOffreRp.competance_demande;
    offre.technologies.libele = result.data.saveOffreRp.technologies.libele ;
    offre.technologies.id = result.data.saveOffreRp.technologies.id ;
    offre.user.username =  result.data.saveOffreRp.user.username ;

          /*  rpFild.username = result.data.technologie.text;
                  listeTechnologie.technologie = result.data.technologie.libele ;*/
/*    this.toastr.success('Offre ajouté avec succées', 'Succées');
    this.ajouterOffre.emit(offre);*/

        });

}
}
