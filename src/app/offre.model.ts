import {Technologie} from './technologie.model';
import {User} from './user.model';

export class Offre {
  id: number ;
  organisme: string ;
  description: string;
  effectif: number ;
  etude_besoin: string ;
  diplome: string;
  nb_annee_experience: number;
  exigence: string;
  etat: string;
  competance_demande: string;
  technologies: Technologie ;
  user: User ;

}
