import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {NgForm} from '@angular/forms';
import { ViewChild } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import {User} from '../user.model';
import { HttpClientModule } from '@angular/common/http';
import {login} from '../shared/technologie/query';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  @ViewChild('userForm') form: any;
  constructor(private apollo: Apollo, private  auth: AuthService , private route: Router) {
  }

  onSubmit() {
    console.log(this.username);
    console.log(this.password);
    this.apollo.watchQuery<any>({
      query: login,
      variables: { username: this.username , password: this.password}
    })
      .valueChanges
      .subscribe(result => {
        const user = new User();

        user.token = result.data.user.token ;

       user.username = result.data.user.user.username ;
        user.role = result.data.user.user.role ;
        this.auth.sendUser(user);
        this.auth.sendToken(result.data.user.token);
        this.route.navigate(['/interface/technologie']);


      });
  }
  ngOnInit() {
  }

}




