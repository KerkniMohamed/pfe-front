import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {User} from '../user.model';
@Injectable()
export class AuthService {
  Usc: User;
  x: string;
  constructor(private myRoute: Router) { }
  sendToken(token: string) {
    localStorage.setItem('LoggedInUser', token);
  }
  sendrole(role: string) {
    localStorage.setItem('LoggedInUserROLE', role);
  }
  getrole() {
    this.x = localStorage.getItem('LoggedInUserROLE');
    return atob(this.x);
  }
  sendUser(Us: User) {
    this.Usc = Us;
    localStorage.setItem('test', JSON.stringify( this.Usc));
  }
  getUser() {
    return  JSON.parse(localStorage.getItem('test'));

  }
  getToken() {
    return localStorage.getItem('LoggedInUser');
  }
  isLoggednIn() {
    return this.getToken() !== null;
  }
  logout() {
    localStorage.removeItem('LoggedInUser');
    localStorage.removeItem('LoggedInUserROLE');

    localStorage.removeItem('test');

    this.myRoute.navigate(['login']);
  }
}
