import gql from 'graphql-tag';
const login = gql`
   query user($username:String!,$password:String!) {
    user(username: $username,password: $password){ token, user {username,roles} }
    }
`;
const listeTechnologie = gql`
   query getAllTechnologies {
    getAllTechnologies{  id , libele }
    }` ;
export {
     login,
     listeTechnologie ,
};
