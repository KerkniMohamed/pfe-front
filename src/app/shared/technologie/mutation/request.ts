import gql from 'graphql-tag';

const deleteTechnologie = gql`
mutation deleteTechnologie($id: Int!) {
deleteTechnologie(id : $id)
}
`;
const addTechnologie = gql`
mutation saveTechnologie($libele:String!) {
  saveTechnologie(libele: $libele){
  id ,
libele
  }
}
`;
const updateTechnologie = gql`
mutation updateTechnologie($id: Int! , $libele:String!) {
  updateTechnologie(id : $id , libele: $libele)
}
`;
export {
    deleteTechnologie,
    addTechnologie,
    updateTechnologie,

};
