import gql from 'graphql-tag';

const AddOffre = gql`
mutation saveOffreRp(
$organisme: String! ,
$description: String!,
 $effectif: Int! ,
$etude_besoin: String! ,
 $competance_demande: String! ,
$diplome: String! ,
$nb_annee_experience: Int! ,
$exigence: String! ,
$etat: String! ,
$technologies :TechnologiesOffre!,
$user :RpOffre!)
{
saveOffreRp(organisme: $organisme ,
description: $description,
 effectif: $effectif ,
 etude_besoin: $etude_besoin ,
 competance_demande: $competance_demande ,
diplome: $diplome ,
nb_annee_experience: $nb_annee_experience ,
exigence: $exigence ,
etat: $etat ,
technologies : $technologies,
user: $user)
{
id,
organisme,
description,
effectif
,etude_besoin,
diplome,
  nb_annee_experience,
  exigence,
  etat ,
  technologie {id,libele} ,
  user {id,text}
}
}
`;
export {
  AddOffre,
};

