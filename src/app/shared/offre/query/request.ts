import gql from 'graphql-tag';
const getOffres = gql`
query getOffresRp {
getOffresRp {
id ,
description ,
organisme ,
etat,
technologies { id , libele}
}
}
`;
const rpFild = gql`
query rpFild {
rpFild  {
id ,
username
}
}
`;
export {
 getOffres,
  rpFild
};
