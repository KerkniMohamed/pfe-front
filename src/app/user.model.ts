export class User {
  __typename: string;
  username: string;
  password: string;
  token: string;
  role: string ;
}
