import {NgModule} from '@angular/core';
import {ApolloModule, APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';

const uri = 'http://recrutement-pfe.proxym-it.net/graphql'; // <-- add the URL of the GraphQL server here
const auth = setContext((_, { headers }) => {
// get the authentication token from local storage if it exists
  const token = localStorage.getItem('LoggedInUser');

// return the headers to the context so httpLink can read them
// in this example we assume headers property exists
// and it is an instance of HttpHeaders
  if (!token) {
    return {};
  } else {
    return {
      headers: {
        Authorization: `Bearer ${token}`
      }

    };
  }
});

export function createApollo(httpLink: HttpLink) {
  const defaultsOptions = {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    }
  } ;
  const http = httpLink.create({uri});
  return {
    link: auth.concat(http),
    cache: new InMemoryCache(),
    defaultOptions: defaultsOptions,
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
})
export class GraphQLModule {

}
